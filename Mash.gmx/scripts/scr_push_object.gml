
        
    with(obj_statue)
    {
        if place_meeting(x + 3, y, obj_top_down_player)
        {
            // Pushing blocks left
            if keyboard_check(ord("A"))
            {
                if place_free(x-1, y)
                {
                    x-=1;
                    with(obj_top_down_player)
                    {
                        x += 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x-1, y, obj_wall)
                            {
                                obj_platform_player.x -= 1;
                            }
                        }
                    }
                }
            }
            // Dragging blocks right
            if keyboard_check(ord("D")) && keyboard_check(vk_shift)
            {
                with(obj_top_down_player)
                {
                    sprite_index = spr_top_down_player_left
                }
                if place_free(x + 12, y)
                {
                    x += 1;
                    with(obj_top_down_player)
                    {
                        x -= 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x+1, y, obj_wall)
                            {
                                obj_platform_player.x += 1;
                            }
                        }
                    }
                }
            }
        }

        else if place_meeting(x - 3, y, obj_top_down_player)
        {
            // Pushing blocks right
            if keyboard_check(ord("D"))
            {
                if place_free(x+1, y)
                {
                    x+=1;
                    with(obj_top_down_player)
                    {
                        x -= 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x+1, y, obj_wall)
                            {
                                obj_platform_player.x += 1;
                            }
                        }
                    }
                }
            }
            // Dragging blocks left
            if keyboard_check(ord("A")) && keyboard_check(vk_shift)
            {
                with(obj_top_down_player)
                {
                    sprite_index = spr_top_down_player_right
                }
                if place_free(x - 12, y)
                {
                    x -= 1;
                    with(obj_top_down_player)
                    {
                        x += 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x-1, y, obj_wall)
                            {
                                obj_platform_player.x -= 1;
                            }
                        }
                    }
                }
            }
        }
        else if place_meeting(x, y - 3, obj_top_down_player)
        {
            // Pushing blocks down
            if keyboard_check(ord("S"))
            {
                if place_free(x, y+1)
                {
                    y += 1;
                    with(obj_top_down_player)
                    {
                        y -= 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x, y+1, obj_wall)
                            {
                                obj_platform_player.y += 1;
                            }
                        }
                    }
                }
            }
            // Dragging blocks up
            if keyboard_check(ord("W")) && keyboard_check(vk_shift)
            {
                with(obj_top_down_player)
                {
                    sprite_index = spr_top_down_player_down
                }
                if place_free(x, y - 12)
                {
                    y -= 1;
                    with(obj_top_down_player)
                    {
                        y += 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x, y-1, obj_wall)
                            {
                                obj_platform_player.y -= 1;
                            }
                        }
                    }
                }
            }
        }
        else if place_meeting(x, y + 3, obj_top_down_player)
        {
            // Pushing blocks up
            if keyboard_check(ord("W"))
            {
                if place_free(x, y-1)
                {
                    y -= 1;
                    with(obj_top_down_player)
                    {
                        y += 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x, y-1, obj_wall)
                            {
                                obj_platform_player.y -= 1;
                            }
                            else
                            {
                                with(collision_rectangle(x+2, y+8, x-2, y+10, obj_statue, false, false))
                                {
                                    y+= 1
                                }
                                with(obj_top_down_player)
                                {
                                    y -= 0.5;
                                }
                            }
                        }
                    }
                }
            }
            // Dragging blocks down
            if keyboard_check(ord("S")) && keyboard_check(vk_shift)
            {
                with(obj_top_down_player)
                {
                    sprite_index = spr_top_down_player_up
                }
                if place_free(x, y + 12)
                {
                    y += 1;
                    with(obj_top_down_player)
                    {
                        y -= 0.5;
                    }
                    if place_meeting(x, y-2, obj_platform_player)
                    {
                        with (obj_platform_player)
                        {
                            if !place_meeting(x, y+1, obj_wall)
                            {
                                obj_platform_player.y += 1;
                            }
                        }
                    }
                }
            }
        }
    }
